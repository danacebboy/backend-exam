<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

use App\Comments;
use App\Posts;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;

class CommentController extends Controller
{
    public function comments($slug=null){
        $qry = DB::select("select id from posts where slug = '$slug'");
        $pid = $qry[0]->id;
        $count = DB::table('comments')->where('commentable_id',$pid)->count();
        if(!is_null($slug)){
            
            $posts = Comments::where('commentable_id',$pid)->orderBy('created_at','desc')->paginate(15);
            if(is_null($posts))
            {
                return response()->json(['message'=>'No query results for model Post.'],404);
            }else{
                return response()->json($posts, 200);
            }
            
        }else{
            
            $posts = Comments::where('commentable_id',$pid)->first();
            if(is_null($posts))
            {
                return response()->json(['message'=>'No query results for model Post.'],404);
            }else{
                return response()->json(["data"=>$posts], 200);
            }
            
        }
    }
    public function addComment(Request $request, $slug=null){
        $validator = Validator::make($request->all(), [
            'body' => 'required|string'
        ],[
            'required' => 'The :attribute field is required.',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['message'=>'The given data was invalid.','errors'=>$errors], 422);
        }
        
        $post = DB::table('posts')->where('slug',$slug)->get()->first();
        if(!empty($post->id)){
            $commentable_id = $post->id;
            $parent_id = (is_null($request->parent_id))?  null : $request->parent_id;
            $comments = new Comments([
                'body' => $request->body,
                'commentable_type' => 'App\\Post',
                'commentable_id' => $commentable_id,
                'parent_id' => $post->id,
                'creator_id' => Auth::id(),
            ]);
            $comments->save();
    
            return response()->json($comments, 201);
        }else{
            $posts = Posts::where('slug', '=', $slug)->firstOrFail();
            return response()->json(['message'=>$posts], 404);
        }
        
        /*  */
    }
    public function updateComment(Request $request,$slug=null,$id=null){
        if (is_null($slug)) {
            return response()->json(['message'=>'The given data was invalid.','errors'=>$errors], 422);
        }
        
        if(!$request->body && !$id){
            return response()->json(["message"=>"There's an error."], 422);
        }
        $posts = DB::table('posts')->where('slug', $slug)->get()->first();
        if($posts)
            DB::statement("update comments set body='".$request->body."' where commentable_id='".$posts->id."' and id=".$id);

        $post = DB::table('comments')->where('id', $id)->get();
        return response()->json(["data"=>$post], 201);
    }
    public function deleteComment(Request $request,$slug=null,$id=null){
        if(!is_null($id))
        {
            $posts = DB::table('posts')->where('slug', $slug)->get()->first();
            DB::delete("delete from comments where id='".$id."' and commentable_id=".$posts->id);
    
            return response()->json(["status"=>"record deleted successfully"], 200);
        }
        
    }
}
