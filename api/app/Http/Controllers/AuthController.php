<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|string|unique:users',
            'password' => 'required|string|same:password_confirmation',
        ],[
            'same' => 'The password confirmation does not match.',
            'required' => 'The :attribute field is required.',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['message'=>'The given data was invalid.','errors'=>$errors], 422);
        }
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $user->save();

        return response()->json($user, 201);
    }
    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'email' => 'required|exists:users',
        ],[
            'exists' => 'These credentials do not match our records.',
            'required' => 'The :attribute field is required.',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['message'=>'The given data was invalid.','errors'=>$errors], 422);
        }

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json(['message'=>'The given data was invalid.','errors'=>['These credentials do not match our records.']], 422);

        $user = Auth::user();
        $tokenResult = $user->createToken('authToken');
        $token = $tokenResult->token;

        $token->expires_at = Carbon::now()->addDays(40);
        $token->save();
        return response()->json([
            'token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
        
    }
    public function check(Request $request){
        echo 'asdfas';
    }
    public function dashboard(Request $request){
        echo 'asdfasf';
    }
    public function logout(Request $request){
            
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

}
