<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

use App\Posts;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostFormRequest;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\DB;

use Auth;
use Validator;

class PostController extends Controller
{
    public function posts($slug=null){
        //fetch 5 posts from database which are active and latest
        $count = DB::table('posts')->where('slug',$slug)->count();
        if(is_null($slug)){
            $posts = Posts::orderBy('created_at','desc')->paginate(15);
            if(is_null($posts))
            {
                return response()->json(['message'=>'No query results for model Post.'],404);
            }else{
                return $result = PostResource::collection($posts);
            }
            
        }else{
            
            $posts = Posts::where('slug',$slug)->first();
            if(is_null($posts))
            {
                return response()->json(['message'=>'No query results for model Post.'],404);
            }else{
                return response()->json(["data"=>$posts], 200);
            }
            
        }
    }
    public function addPost(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts',
            'content' => 'required|string'
        ],[
            'required' => 'The :attribute field is required.',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['message'=>'The given data was invalid.','errors'=>$errors], 422);
        }
        $post = new Posts([
            'title' => $request->title,
            'content' => $request->content,
            'image' => $request->image,
            'slug' => str_replace(" ", "-", $request->title),
            'user_id' => Auth::id(),
        ]);
        $post->save();

        return response()->json($post, 201);
    }
    public function updatePost(Request $request, $slug=null){
        if (is_null($slug)) {
            return response()->json(['message'=>'The given data was invalid.'], 422);
        }
        
        if(!empty($request->title)){
            $data['title'] = "title='".$request->title."'";
            $data['slug'] = "slug='".str_replace(" ", "-", $request->title."'");
        }
        if(!empty($request->content)) $data['content'] = "content='".$request->content."'";
        if(!empty($request->image)) $data['image'] = "image='".$request->image."'";
        DB::statement("update posts set ".implode(",",$data)." where slug='".$slug."'");
        $slug = (!empty($request->title))? str_replace(" ", "-", $request->title): $slug;
        $users = DB::table('posts')->where('slug', $slug)->get();
        return response()->json(["data"=>$users], 201);
    }
    public function deletePost($slug=null){
        if(!is_null($slug))
        {
            DB::delete("delete from posts where slug='".$slug."'");
    
            return response()->json(["status"=>"record deleted successfully"], 200);
        }
        
    }
}
