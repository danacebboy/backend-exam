<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Auth::routes();

/* Route::group(['middleware' => ['web']], function () {
    //
    }); */
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::get('posts/{slug?}', [PostController::class,'posts']);
    Route::get('posts/{slug?}/comments', [CommentController::class,'comments']);
    
    
    // check for logged in user
Route::middleware(['auth:api'])->group(function () {
    Route::post('logout', 'AuthController@logout');
    Route::get('check', 'AuthController@check');
    Route::get('dashboard', 'AuthController@dashboard');
    //post
    Route::post('posts', 'PostController@addPost');
    Route::delete('posts/{slug?}', [PostController::class,'deletePost']);
    Route::patch('posts/{slug?}', [PostController::class,'updatePost']);

    //comment
    Route::post('posts/{slug?}/comments', [CommentController::class,'addComment']);
    Route::patch('posts/{slug?}/comments/{id?}', [CommentController::class,'updateComment']);
    Route::delete('posts/{slug?}/comments/s', [CommentController::class,'deleteComment']);
    //Route::delete('posts/{id?}', [CommentController::class,'deleteComment']); 
  });